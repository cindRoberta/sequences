# Progressão Aritmética

Uma **progressão aritmética** (abreviadamente, **P. A.**) ou **sequência aritmética** é uma sequência numérica em que cada termo, a partir do segundo, é igual à soma do termo anterior com uma constante $`r`$. O número $`r`$ é chamado de **razão** ou **diferença comum** da progressão aritmética.

Uma parte finita de uma progressão aritmética é chamada uma progressão aritmética finita ou apenas progressão aritmética. A soma de uma progressão aritmética finita é chamada uma série aritmética finita.

## Definição
Notamos que, de modo geral, uma sequência numérica $`\overset{r}{\underset{n~\in~Range}{(a_n)}} = (\ldots,~a_1,~a_2,~a_3,~a_4,~\ldots,~a_{n-2},~a_{n-1},~a_n,~\ldots)`$ é uma P.A. quando definida recursivamente por:

$`\qquad \ldots`$

$`\qquad a_2 = a_1 + r \quad \Rightarrow \quad r = a_2 - a_1`$

$`\qquad a_3 = a_2 + r \quad \Rightarrow \quad r = a_3 - a_2`$

$`\qquad a_4 = a_3 + r \quad \Rightarrow \quad r = a_4 - a_3`$

$`\qquad \ldots`$

$`\qquad a_{n-2} = a_{n-3} + r \quad \Rightarrow \quad r = a_{n-2} - a_{n-3}`$

$`\qquad a_{n-1} = a_{n-2} + r \quad \Rightarrow \quad r = a_{n-1} - a_{n-2}`$

$`\qquad a_n = a_{n-1} + r \quad \Rightarrow \quad r = a_n - a_{n-1}`$

$`\qquad \ldots`$

Comparando, temos:

$`\qquad r = \ldots = a_2 - a_1 = a_3 - a_2 = a_4 - a_3 = \ldots = a_{n-1} - a_{n-2} = a_n - a_{n-1} = \ldots`$

onde
- $`a_n`$ é o *n*-ésimo termo da sequência $`(a_n)`$,
- $`n`$ corresponde ao número de termos (até $`a_n`$)
- o primeiro termo, $`a_1`$ é um número dado,
- o número $`r`$ é chamado de razão da progressão aritmética.

### Exemplos
Alguns exemplos de progressões aritméticas:

- $`\overset{r~=~3}{\underset{n~=~1:\infty}{(a_n)}} = (1,~4,~7,~10,~13,~\ldots)`$ é uma progressão aritmética em que o primeiro termo $`a_1`$ é igual a $`1`$ e a razão $`r`$ é igual a $`3`$.

- $`\overset{r~=~-2}{\underset{n~=~1:\infty}{(a_n)}} = (-2,~-4,~-6,~-8,~-10,~\ldots)`$ é uma P.A. em que $`a_1 = -2`$ e $`r = -2`$.

- $`\overset{r~=~0}{\underset{n~=~1:\infty}{(a_n)}} = (6,~6,~6,~6,~6,~\ldots)`$ é uma P.A. com $`a_1 = 6`$ e $`r = 0`$.

## Fórmula do termo geral
O *n*-ésimo termo de uma progressão aritmética, denotado por $`a_n`$, pode ser obtido por meio da fórmula:

$`\qquad a_n = a_m + (n - m) \cdot r`$

portanto uma sequência $`\overset{r}{\underset{n~\in~Range}{(a_n)}}`$ também pode ser expressa por $`\underset{n~\in~Range}{(a_m + (n - m) \cdot r)}`$

ou

$`\qquad a_n = a_1 + (n - 1) \cdot r`$

de forma semelhante, uma sequência $`\overset{r}{\underset{n~\in~Range}{(a_n)}}`$ também pode ser expressa por $`\underset{n~\in~Range}{(a_1 + (n - 1) \cdot r)}`$

em que:

- $`a_n`$ é o *n*-ésimo termo da sequência $`(a_n)`$
- $`n`$ é o número de termos (até $`a_n`$)
- $`a_1`$ é o primeiro termo;
- $`r`$ é a razão.

### Exemplos

- $`\overset{r~=~3}{\underset{n~=~1:\infty}{(a_n)}} = \overset{m~=~1 \quad r~=~3}{\underset{n~=~1:\infty}{(a_m + (n - m) \cdot r)}} = \underset{n~=~1:\infty}{(a_1 + (n - 1) \cdot 3)} = \underset{n~=~1:\infty}{(1 + (n - 1) \cdot 3)} = \underset{n~=~1:\infty}{(1 + 3n - 3)} = \underset{n~=~1:\infty}{(3n - 2)} = (1,~4,~7,~10,~13,~\ldots)`$

- $`\overset{r~=~-2}{\underset{n~=~1:\infty}{(a_n)}} = \overset{m~=~3 \quad r~=~-2}{\underset{n~=~1:\infty}{(a_m + (n - m) \cdot r)}} = \underset{n~=~1:\infty}{(a_3 + (n - 3) \cdot -2)} = \underset{n~=~1:\infty}{(-6 + (n - 3) \cdot -2)} = \underset{n~=~1:\infty}{(-6 - 2n + 6)} = \underset{n~=~1:\infty}{(-2n)} = (-2,~-4,~-6,~-8,~-10,~\ldots)`$

- $`\overset{r~=~0}{\underset{n~=~1:\infty}{(a_n)}} = \overset{m~=~5 \quad r~=~0}{\underset{n~=~1:\infty}{(a_m + (n - m) \cdot r)}} = \underset{n~=~1:\infty}{(a_5 + (n - 5) \cdot 0)} = \underset{n~=~1:\infty}{(6)} = (6,~6,~6,~6,~6,~\ldots)`$

### Demonstração
#### $`a_n = a_1 + (n - 1) \cdot r`$
Podemos escrever uma Progressão Aritmética da forma geral, tomando como referência o 1º índice:

$`\begin{matrix} \qquad
(\ldots, & a_1, & \underbrace{a_1 + r}, & \underbrace{a_1 + 2r}, & \underbrace{a_1 + 3r}, & \ldots, & \underbrace{a_1 + (n - 3) \cdot r}, & \underbrace{a_1 + (n - 2) \cdot r}, & \underbrace{a_1 + (n - 1) \cdot r}, & \ldots) \\
& & a_2 & a_3 & a_4 & & a_{n-2} & a_{n-1} & a_n
\end{matrix}`$

assim obtendo a primeira fórmula para P.A.

$`\qquad a_n = a_1 + (n - 1) \cdot r`$

ao passar de $`a_1`$ para $`a_n`$, avançamos $`(n - 1)`$ termos, ou seja, basta somar $`(n - 1)`$ vezes a razão ao $`1º`$ termo.

#### Demonstração por indução matemática
A fórmula do termo geral pode ser demonstrada por indução matemática:

- Ela é válida para o segundo termo pois, por definição, cada termo é igual ao anterior mais uma constante fixa $`r`$ e portanto $`a_2 = a_1 + 1 \cdot r`$
- Assumindo como hipótese de indução que a fórmula é válida para $`n - 1`$, ou seja, que $`a_{n-1} = a_1 + (n - 2) \cdot r`$, resulta que o *n*-ésimo termo é dado por

$`\qquad a_n = a_{n-1} + r = (a_1 + (n - 2) \cdot r) + r = a_1 + (n - 2 + 1) \cdot r = a_1 + (n - 1) \cdot r`$

#### $`a_n = a_m + (n - m) \cdot r`$
#### $`a_{n-p} = a_m + (n - (m + p)) \cdot r`$
tomando como referência outros índices, por exemplo, 2 e 3:

$`\begin{matrix} \qquad
(\ldots, & a_1, & a_2, & \underbrace{a_2 + r}, & \underbrace{a_2 + 2r}, & \underbrace{a_2 + 3r}, & \ldots, & \underbrace{a_2 + (n - 4) \cdot r}, & \underbrace{a_2 + (n - 3) \cdot r}, & \underbrace{a_2 + (n - 2) \cdot r}, & \ldots) \\
& & & a_3 & a_4 & a_5 & & a_{n-2} & a_{n-1} & a_n
\end{matrix}`$

$`\begin{matrix} \qquad
(\ldots, & a_1, & a_2, & a_3, & \underbrace{a_3 + r}, & \underbrace{a_3 + 2r}, & \underbrace{a_3 + 3r}, & \ldots, & \underbrace{a_3 + (n - 5) \cdot r}, & \underbrace{a_3 + (n - 4) \cdot r}, & \underbrace{a_3 + (n - 3) \cdot r}, & \ldots) \\
& & & & a_4 & a_5 & a_6 & & a_{n-2} & a_{n-1} & a_n
\end{matrix}`$

observe que qualquer que seja o índice tomado como referência, são sempre válidas e dedutíveis as fórmulas:

$`\qquad a_n = a_m + (n - m) \cdot r`$

ao passar de $`a_m`$ para $`a_n`$, avançamos $`(n - m)`$ termos, ou seja, basta somar $`(n - m)`$ vezes a razão ao $`mº`$ termo

$`\qquad a_{n-p} = a_m + (n - (m + p)) \cdot r`$

Note que:
- $`a_9 = a_4 + 5r`$, pois, ao passar de $`a_4`$ para $`a_9`$, avançamos 5 termos

- $`a_3 = a_{15} - 12r`$, pois retrocedemos 12 termos ao passar de $`a_{15}`$ para $`a_3`$


#### $`~a_n = a_{n-p} + pr`$
$`\qquad a_n = a_{n-1} + r = a_{n-2} + 2r = a_{n-3} + 3r = a_{n-p} + pr`$

$`\qquad a_n = a_{n-p} + pr`$

#### Referências
https://pt.khanacademy.org/math/algebra2/sequences-and-series/alg2-seq-and-series/v/formula-for-arithmetic-series
